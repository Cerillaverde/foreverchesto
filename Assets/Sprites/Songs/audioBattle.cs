using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class audioBattle : MonoBehaviour
{
    public List<AudioClip> sources;
    // Start is called before the first frame update
    void Start()
    {
        this.GetComponent<AudioSource>().clip = sources[Random.Range(0, sources.Count)];
        this.GetComponent<AudioSource>().Play();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
