using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Tilemaps;

public class GameManager : MonoBehaviour
{

    public static GameManager Instance = null;

    // Instanciamos el tiles y el pathfinding
    public GameObject player;
    public Tilemap sceneTileMap;

    //  Lista de Cartas
    public List<CardsScriptable> allMobs = new List<CardsScriptable>();
    [HideInInspector]
    public List<CardsScriptable> myTeam = new List<CardsScriptable>();

    // Control sobre la batalla
    [HideInInspector]
    public string jugador;
    [HideInInspector]
    public int level;
    [HideInInspector]
    public bool runBattle;
    [HideInInspector]
    public int jewel;
    [HideInInspector]
    public bool zonaVenta;
    [HideInInspector]
    public bool canBuy;
    [HideInInspector]
    public int reroll;
    [HideInInspector]
    public int timeToBattle;
    [HideInInspector]
    public int playerWin;
    [HideInInspector]
    public int oponentWin;
    [HideInInspector]
    public int round;
    // Lista de posiciones en la batalla
    [HideInInspector]
    public List<Vector3Int> enemyPosition = new List<Vector3Int>();
    [HideInInspector]
    public List<Vector3Int> aliesPosition = new List<Vector3Int>();

    // Listas que contienen los gameobjects que  estan en el tablero
    [HideInInspector]
    public List<GameObject> enemyMobs = new List<GameObject>();
    [HideInInspector]
    public List<GameObject> aliesMobs = new List<GameObject>();

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            //DontDestroyOnLoad(this);
        }
        else if (Instance != this)
        {
            Destroy(gameObject);
        }

        //SceneManager.LoadScene("Inicio");

    }

    // Start is called before the first frame update
    private void Start()
    {
        //SceneManager.LoadScene("Inicio");
    }

}
