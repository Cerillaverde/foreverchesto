using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
using UnityEngine.SceneManagement;

public class BattleController : MonoBehaviour
{
    public buttonController playButton;
    public GameObject player;
    public GameObject time;
    public GameObject cards;
    public DatosPersistentes datos;
    public AudioClip lucas;

    // Start is called before the first frame update
    void Start()
    {
        // Asignar variables
        GameManager.Instance.runBattle = false;
        GameManager.Instance.jewel = 6;
        GameManager.Instance.canBuy = true;
        GameManager.Instance.playerWin = -1;
        GameManager.Instance.oponentWin = -1;
        GameManager.Instance.round = 0;
        GameManager.Instance.level = datos.nivell + 1;

        GameManager.Instance.myTeam = datos.myTeam;
        GameManager.Instance.jugador = datos.nombre;

        // Courutines
        GameManager.Instance.timeToBattle = 10 + (2 * GameManager.Instance.round);
        StartCoroutine(TimeToBattle(GameManager.Instance.timeToBattle));
    }

    // Update is called once per frame
    void Update()
    {
        if (GameManager.Instance.runBattle == true && GameManager.Instance.aliesMobs.Count == 0)
        {
            GameManager.Instance.oponentWin++;
            ResetBattle();
        }
        if (GameManager.Instance.runBattle == true && GameManager.Instance.enemyMobs.Count == 0)
        {
            GameManager.Instance.playerWin++;
            ResetBattle();
        }

        if (GameManager.Instance.oponentWin == 2)
        {
            SceneManager.LoadScene("General");
        }
        else if(GameManager.Instance.playerWin == 2)
        {
            datos.nivell++;
            SceneManager.LoadScene("General");
        }

        if (GameManager.Instance.timeToBattle < 0)
        {
            GameManager.Instance.timeToBattle = 0;
        }

    }



    private void ResetBattle()
    {
        // Reset UI
        playButton.gameObject.SetActive(true);
        playButton.GetComponent<buttonController>().cards.gameObject.SetActive(true);
        playButton.GetComponent<buttonController>().refresh.gameObject.SetActive(true);
        playButton.GetComponent<buttonController>().mana.gameObject.SetActive(true);
        playButton.GetComponent<buttonController>().tilemap.gameObject.SetActive(true);
        playButton.GetComponent<buttonController>().cost1.gameObject.SetActive(true);
        playButton.GetComponent<buttonController>().cost2.gameObject.SetActive(true);
        playButton.GetComponent<buttonController>().cost3.gameObject.SetActive(true);

        CleanGrid();

        // Poner aqui todo el tema de animacion acabar la ronda

        //Update variables
        GameManager.Instance.round++;
        GameManager.Instance.jewel = 6 + (2 * GameManager.Instance.round);
        GameManager.Instance.reroll += 1;
        GameManager.Instance.timeToBattle = 10 + (2 * GameManager.Instance.round);
        StartCoroutine(TimeToBattle(GameManager.Instance.timeToBattle));
        cards.GetComponent<cardController>().UpdateBooking();

        //Activar variable creada en singleton de battleRun
        GameManager.Instance.runBattle = false;
        CleanGrid();
        player.GetComponent<InstanciateMobs>().InstanciateHero();

    }

    public IEnumerator TimeToBattle(int time)
    {
        for (int i = 0; i < time; i++)
        {
            yield return new WaitForSeconds(1);
            GameManager.Instance.timeToBattle--;
        }
        if (!GameManager.Instance.runBattle)
        {
            playButton.GetComponent<buttonController>().ClickPlay();
        }
    }

    public void ResetTimeToBattle()
    {
        StopAllCoroutines();
    }

    private void CleanGrid()
    {

        for (int i = 0; i < GameManager.Instance.aliesMobs.Count; i++)
        {
            GameObject coso = GameManager.Instance.aliesMobs[i].gameObject;
            Destroy(coso);
        }
        for (int i = 0; i < GameManager.Instance.enemyMobs.Count; i++)
        {
            GameObject coso = GameManager.Instance.enemyMobs[i].gameObject;
            Destroy(coso);
        }
        GameManager.Instance.aliesMobs.Clear();
        GameManager.Instance.enemyMobs.Clear();
        GameManager.Instance.aliesPosition.Clear();
        GameManager.Instance.enemyPosition.Clear();


        // Medida drastica por el fucking bug TERRORISMO AWESOME, MAGIA IMPURA
        // Almenos tenemos medidas de seguridad, gracias Alberto... ba dum tss
        int contador = 0;
        while (true)
        {
            if (GameObject.Find("GeneralMob(Clone)") != null)
            {
                GameObject restante = GameObject.Find("GeneralMob(Clone)");
                DestroyImmediate(restante);
                contador++;
                if (contador == 10)
                {
                    break;
                }
            }
            else
            {
                break;
            }
        }
        //AQUI EN MEDIO YO NO VEO NADA
        //SIGUE NADANDO...

    }

}
