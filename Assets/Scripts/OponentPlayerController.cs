
using System.Collections.Generic;
using UnityEngine;

public class OponentPlayerController : MonoBehaviour
{
    public DatosPersistentes level;
    List<string> names = new List<string>();
    // Start is called before the first frame update
    void Start()
    {
        names.Add("Arbolito\n Autista");
        names.Add("Etharoth\n Senior de la Corrupcion");
        names.Add("Anmariu'th\n Reina del Inframundo");
        names.Add("Zelhene\n Emperatriz de la humildad");
        names.Add("Lucas\n Amante de Greg'Odoo");
        names.Add("Greg'Odoo\n Sentenciador Modular");
        names.Add("Dan Ikeburo\n Muerte Sistematica");
        names.Add("Pantonio\n Sombra de Z'bruh");
        names.Add("Albareda\n Moral de Jav'Uni");
        names.Add("Vu'eloi\n La piedra Angular");

        this.GetComponent<TMPro.TextMeshProUGUI>().text = names[level.nivell];
    }

    // Update is called once per frame
    void Update()
    {
        for (int i = 0; i < this.transform.childCount; i++)
        {
            if (GameManager.Instance.oponentWin == i && this.transform.GetChild(i).gameObject.active == false)
            {
                this.transform.GetChild(i).gameObject.SetActive(true);
            }
        }
    }
}
