using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class chetos : MonoBehaviour
{

    public GameObject Manager;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void destruccuion()
    {
        Destroy(GameObject.Find("GeneralMob(Clone)"));
        if(GameObject.Find("GeneralMob(Clone)") == null)
        {
            Debug.Log("null tal cual");
        }
    }

    public void moreTime()
    {
        GameManager.Instance.timeToBattle += 100;
        Manager.GetComponent<BattleController>().ResetTimeToBattle(); 
        Manager.GetComponent<BattleController>().StartCoroutine(Manager.GetComponent<BattleController>().TimeToBattle(GameManager.Instance.timeToBattle));
    }

    public void moreJewel()
    {
        GameManager.Instance.jewel += 50;
    }

    public void moreRolls()
    {
        GameManager.Instance.reroll += 90;
    }

    public void levelUp()
    {
        GameManager.Instance.playerWin++;
        GameManager.Instance.oponentWin = 0;
    }

}
