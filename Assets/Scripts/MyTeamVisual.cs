using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MyTeamVisual : MonoBehaviour
{

    public DatosPersistentes datos;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        for (int i = 0; i < this.transform.childCount; i++)
        {
            this.transform.GetChild(i).GetComponent<CardDisplay>().Card = datos.myTeam[i];
        }

        if (datos.nivell == 10)
        {
            SceneManager.LoadScene("End");
        }
    }
}
