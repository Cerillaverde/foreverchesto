using System.Collections;
using System.Collections.Generic;
using Microsoft.Unity.VisualStudio.Editor;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.UIElements;

public class GestionIntermedio : MonoBehaviour
{
    public UnityEngine.UI.Image oponente;
    public Sprite[] backg;
    public DatosPersistentes datos;
    public Sprite[] fotos;
    public TMP_Text nombre;
    public List<AudioClip> sources;
    public AudioSource sonidos;

    List<string> names = new List<string>();
    // Start is called before the first frame update
    void Start()
    {

        this.GetComponent<UnityEngine.UI.Image>().sprite = backg[Random.Range(0, 2)];

        names.Add("Arbolito\n Autista");
        names.Add("Etharoth\n Senior de la Corrupcion");
        names.Add("Anmariu'th\n Reina del Inframundo");
        names.Add("Zelhene\n Emperatriz de la humildad");
        names.Add("Lucas\n Amante de Greg'Odoo");
        names.Add("Greg'Odoo\n Sentenciador Modular");
        names.Add("Dan Ikeburo\n Muerte Sistematica");
        names.Add("Pantonio\n Sombra de Z'bruh");
        names.Add("D.Albareda\n Moral de Jav'Uni");
        names.Add("Vu'eloi\n La piedra Angular");

        nombre.text = names[datos.nivell];
        //this.GetComponent<UnityEngine.UI.Image>().sprite = fotos[0];
        StartCoroutine(putImage());

        StartCoroutine(NewScene());

    }

    // Update is called once per frame
    void Update()
    {

    }

    IEnumerator putImage()
    {
        yield return new WaitForSeconds(1);
        oponente.color = Color.white;
        oponente.sprite = fotos[datos.nivell];
        sonidos.clip = sources[datos.nivell];
        sonidos.Play();
        //this.GetComponent<AudioSource>().Play();
    }

    IEnumerator NewScene()
    {
        yield return new WaitForSeconds(5);
        SceneManager.LoadScene("Batalla");
    }
}
