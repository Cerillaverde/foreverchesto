using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelGestion : MonoBehaviour
{
    public DatosPersistentes level ;

    // Start is called before the first frame update
    void Start()
    {
        this.GetComponent<TMPro.TextMeshProUGUI>().text = "Level: " + level.nivell;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}

