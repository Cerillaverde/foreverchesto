using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cardController : MonoBehaviour
{

    // Start is called before the first frame update


    void Start()
    {
        GameManager.Instance.reroll = 1;
        UpdateBooking();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void UpdateBooking()
    {
        if (GameManager.Instance.reroll > 0)
        {
            for (int i = 0; i < 3; i++)
            {
                int randomNumber = Random.Range(1, 6);
                GameObject son = this.transform.GetChild(i).gameObject;
                CardsScriptable cardMob = GameManager.Instance.myTeam[randomNumber];

                son.GetComponent<SpriteRenderer>().sprite = cardMob.img[0];
                son.GetComponent<cardInvokeController>().id = cardMob.id;
            }
        }
    }

    public void Reroll()
    {
        GameManager.Instance.reroll -= 1;
        if (GameManager.Instance.reroll <=0)
        {
            GameManager.Instance.reroll = 0;
        }
    }

}
