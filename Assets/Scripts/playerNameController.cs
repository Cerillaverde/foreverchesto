using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class playerNameController : MonoBehaviour
{

    public TMP_Text textPlayerName;
    public DatosPersistentes datos;

    // Start is called before the first frame update
    void Start()
    {
        textPlayerName.text = datos.nombre;
        if (datos.nombre == "")
        {
            textPlayerName.text = "Testing";
        }
    }

    // Update is called once per frame
    void Update()
    {
        for (int i = 0; i < this.transform.childCount; i++)
        {
            if (GameManager.Instance.playerWin == i && this.transform.GetChild(i).gameObject.active == false)
            {
                this.transform.GetChild(i).gameObject.SetActive(true);
            }
        }
    }

}
