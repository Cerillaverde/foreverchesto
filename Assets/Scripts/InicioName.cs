using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor.SearchService;
using UnityEngine;
using UnityEngine.SceneManagement;

public class InicioName : MonoBehaviour
{
    public DatosPersistentes datos;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (datos.nombre == "")
        {
            this.GetComponent<TMPro.TextMeshProUGUI>().text = "Testing";
        }
        else
        {
            this.GetComponent<TMPro.TextMeshProUGUI>().text = datos.nombre;
        }
    }

    public void EnterGame()
    {
        SceneManager.LoadScene("Intermedio");
    }

}
