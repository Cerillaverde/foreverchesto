using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class timeCtrl : MonoBehaviour
{

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        this.GetComponent<TMPro.TextMeshProUGUI>().text = "" + GameManager.Instance.timeToBattle;
        if (GameManager.Instance.runBattle)
        {
            GameManager.Instance.timeToBattle = 0;
        }
    }

}
