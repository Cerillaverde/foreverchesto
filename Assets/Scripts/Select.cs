using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Select : MonoBehaviour
{
    public DatosPersistentes datos;
    public Canvas teamCanvas;
    public Canvas canvas;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        for (int i = 0; i < this.datos.allCards.Count; i++)
        {
            if (!this.datos.myTeam.Contains(this.datos.allCards[i]) && !this.datos.otherCards.Contains(this.datos.allCards[i]))
            {
                this.datos.otherCards.Add(this.datos.allCards[i]);
            }

        }

        for (int i = 0; i < this.transform.childCount - 1; i++)
        {
            this.transform.GetChild(i).gameObject.GetComponent<CardDisplay>().Card = this.datos.otherCards[i];
        }
    }

    public void backCanv()
    {
        this.transform.gameObject.SetActive(false);
        teamCanvas.transform.gameObject.SetActive(true);

        for (int i = 1; i < canvas.transform.childCount; i++)
        {
            canvas.transform.GetChild(i).transform.gameObject.SetActive(true);
        }

    }
}
