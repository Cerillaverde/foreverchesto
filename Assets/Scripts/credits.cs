using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class credits : MonoBehaviour
{
    // Start is called before the first frame update
    public float velocidad = 20f; // Velocidad de desplazamiento de los cr�ditos
    public RectTransform rectTransform; // Referencia al componente RectTransform del objeto "Creditos"
    private bool delay;

    private void Start()
    {
        rectTransform = GetComponent<RectTransform>();
        delay = false;
        StartCoroutine(delayed());
        StartCoroutine(newGame());
    }

    private void Update()
    {
        if (delay)
            rectTransform.position += Vector3.up * velocidad * Time.deltaTime;

    }

    IEnumerator newGame()
    {
        yield return new WaitForSeconds(200);
        SceneManager.LoadScene("Inicio");
    }

    IEnumerator delayed()
    {
        yield return new WaitForSeconds(3);
        delay = true;
    }

}
